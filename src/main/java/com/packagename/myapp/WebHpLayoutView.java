package com.packagename.myapp;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HasDynamicTitle;

@CssImport("./styles/shared-styles.css")
@CssImport("./screen.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
public class WebHpLayoutView extends VerticalLayout implements HasDynamicTitle {

	protected VerticalLayout base;
	protected HorizontalLayout header;
	protected VerticalLayout middle_first;
	protected VerticalLayout middle_second;
	protected HorizontalLayout footer;

	protected FooterComponent footPanel;

	public WebHpLayoutView() {

		this.setClassName("page-container");
		this.setMargin(false);
		this.setPadding(false);
		this.setSpacing(false);
		this.setSizeFull();

		// base panel

		this.base = new VerticalLayout();
		this.base.addClassName("layout-base");
		this.base.setSpacing(false);
		this.base.setPadding(false);
		this.base.setSpacing(false);

		// header

		this.header = new HorizontalLayout();

		this.header.setWidth("100%");
		this.header.setMaxWidth("1200px");
		this.header.getStyle().set("marginLeft", "auto");
		this.header.getStyle().set("marginRight", "auto");
		this.header.addClassName("layout-header");
		this.header.getElement().setProperty("innerHTML", "<h1>Portál 3D tisku</h1>");

		// middle first

		this.middle_first = new VerticalLayout();

		this.middle_first.setWidth("100%");
		this.middle_first.setMaxWidth("1200px");
		this.middle_first.getStyle().set("marginLeft", "auto");
		this.middle_first.getStyle().set("marginRight", "auto");
		this.middle_first.addClassName("layout-middle-first");

		// middle second

		this.middle_second = new VerticalLayout();

		this.middle_second.setWidth("100%");
		this.middle_second.setMaxWidth("1200px");
		this.middle_second.getStyle().set("marginLeft", "auto");
		this.middle_second.getStyle().set("marginRight", "auto");
		this.middle_second.addClassName("layout-middle-second");

		// foot panel

		this.footer = new HorizontalLayout();

		this.footer.setMargin(false);
		this.footer.setPadding(false);
		this.footer.setSpacing(false);
		this.footer.setWidth("100%");
		this.footer.addClassName("layout-footer");

		this.footer.add(this.footPanel = new FooterComponent());

		// append

		this.base.add(this.header, this.middle_first, this.middle_second);
		this.add(this.base, this.footer);

	}

	@Override
	public String getPageTitle() {
		return "Portál 3D tisku";
	}

	protected HorizontalLayout getHeader() {
		return header;
	}

	protected VerticalLayout getMiddle_first() {
		return middle_first;
	}

	protected VerticalLayout getMiddle_second() {
		return middle_second;
	}

	protected HorizontalLayout getFooter() {
		return footer;
	}

}
