package com.packagename.myapp;

import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.treegrid.TreeGrid;

import java.util.List;

public class WebLeftMenu extends VerticalLayout {

	private TreeGrid<MenuItem> grid;

	private MenuCollector menu;
	private MenuItem currentMenu;

	public WebLeftMenu(MenuCollector menu) {

		this.menu = menu;

		this.setMargin(false);
		this.setPadding(false);

		this.grid = new TreeGrid<>();
		this.grid.addClassName("left-menu");
		this.grid.setItems(this.menu.getRootChilds(), MenuItem::getChilds);
		this.grid.addHierarchyColumn(MenuItem::getCaption);
		this.grid.setUniqueKeyDataGenerator("key", MenuItem::getKeyPath);
		this.grid.addItemClickListener(event -> {

			this.setCurrentMenu(event.getItem());

			Notification.show("Clicked: " + event.getItem().getCaption() + " | " + event.getItem().getKeyPath());

			UI.getCurrent().navigate(event.getItem().getKeyPath());

		});

		List<HasElement> views = UI.getCurrent().getInternals().getActiveRouterTargetsChain();

		this.add(this.grid);
	}

	public MenuItem getCurrentMenu() {
		return this.currentMenu;
	}

	public void setCurrentMenu(MenuItem currentMenu) {
		this.grid.select(this.currentMenu = currentMenu);
		this.grid.expand(this.currentMenu.getParent());

	}

}
