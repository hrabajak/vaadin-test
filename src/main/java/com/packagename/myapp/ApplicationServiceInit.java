package com.packagename.myapp;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;

public class ApplicationServiceInit implements VaadinServiceInitListener {

	@Override
	public void serviceInit(ServiceInitEvent event) {

		Image img = new Image("img/img.jpg", "img");

		RouteConfiguration conf = RouteConfiguration.forApplicationScope();

		conf.setRoute("", WebHomepageView.class);
		conf.setRoute("", WebMenuItemView.class);
		conf.setRoute("notfound", WebNotFoundView.class);

	}

}
