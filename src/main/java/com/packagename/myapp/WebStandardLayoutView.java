package com.packagename.myapp;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HasDynamicTitle;
import org.springframework.beans.factory.annotation.Autowired;

@CssImport("./styles/shared-styles.css")
@CssImport("./screen.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
public class WebStandardLayoutView extends VerticalLayout implements HasDynamicTitle {

	protected VerticalLayout base;
	protected HorizontalLayout middle;
	protected HorizontalLayout footer;

	protected VerticalLayout leftPanel;
	protected VerticalLayout rightPanel;

	protected LogoComponent logoPanel;
	protected WebLeftMenu menuPanel;
	protected NavComponent navPanel;
	protected FooterComponent footPanel;

	protected MenuCollector menu;
	protected MenuItem currentMenu;

	public WebStandardLayoutView(@Autowired MenuCollector menu) {

		this.setClassName("page-container");
		this.setMargin(false);
		this.setPadding(false);
		this.setSpacing(false);
		this.setSizeFull();

		// base panel

		this.base = new VerticalLayout();
		this.base.addClassName("layout-base");
		this.base.setSpacing(false);
		this.base.setPadding(false);
		this.base.setSpacing(false);

		// middle panel

		this.middle = new HorizontalLayout();

		this.middle.setMargin(false);
		this.middle.setPadding(false);
		this.middle.setSpacing(false);
		this.middle.setWidth("100%");
		this.middle.setMinHeight("400px");
		this.middle.setMaxWidth("1200px");
		this.middle.getStyle().set("marginLeft", "auto");
		this.middle.getStyle().set("marginRight", "auto");
		this.middle.addClassName("layout-middle");

		this.leftPanel = new VerticalLayout();
		this.rightPanel = new VerticalLayout();

		this.leftPanel.setMaxWidth("300px");
		this.leftPanel.setHeight("100%");
		this.leftPanel.addClassName("layout-leftPanel");
		this.leftPanel.setMargin(false);
		this.leftPanel.setPadding(false);
		this.leftPanel.setSpacing(false);

		this.rightPanel.setWidth("100%");
		this.rightPanel.setHeight("100%");
		this.rightPanel.addClassName("layout-rightPanel");
		this.rightPanel.setMargin(false);
		this.rightPanel.setPadding(false);

		this.middle.add(this.leftPanel, this.rightPanel);

		// logo panel

		this.leftPanel.add(this.logoPanel = new LogoComponent());

		// menu panel

		this.leftPanel.add(this.menuPanel = new WebLeftMenu(this.menu = menu));

		// nav panel

		this.rightPanel.add(this.navPanel = new NavComponent());

		this.navPanel.addDefaultItem();
		this.navPanel.rebuild();

		// foot panel

		this.footer = new HorizontalLayout();

		this.footer.setMargin(false);
		this.footer.setPadding(false);
		this.footer.setSpacing(false);
		this.footer.setWidth("100%");
		this.footer.addClassName("layout-footer");

		this.footer.add(this.footPanel = new FooterComponent());

		// append

		this.base.add(this.middle);
		this.add(this.base, this.footer);

	}

	@Override
	public String getPageTitle() {
		return (this.currentMenu != null ? (this.currentMenu.getCaptionPath() + " | ") : "") + "Portál 3D tisku";
	}

	protected HorizontalLayout getMiddle() {
		return middle;
	}

	protected HorizontalLayout getFooter() {
		return footer;
	}

	protected VerticalLayout getLeftPanel() {
		return leftPanel;
	}

	protected VerticalLayout getRightPanel() {
		return rightPanel;
	}

}
