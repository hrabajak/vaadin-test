package com.packagename.myapp;

import com.vaadin.flow.router.RouteConfiguration;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Stack;

@Service
public class MenuCollector implements Serializable {

	private MenuItem root;

	public MenuCollector() {
		this.root = new MenuItem("root", "");
		MenuItem ch = null;

		this.root.addChild(new MenuItem("Návrh modelů pro 3D tisk", "navrh-modelu-pro-3d-tisk"));
		this.root.addChild(new MenuItem("Tisk vlastního 3D modelu", "tisk-vlastniho-3d-modelu"));
		ch = this.root.addChild(new MenuItem("Katalog reklamních předmětů", "katalog-reklamnich-predmetu"));

		ch.addChild(new MenuItem("Nástroje", "nastroje"));
		ch.addChild(new MenuItem("Tvary", "tvary"));
		ch.addChild(new MenuItem("Hračky", "hracky"));

		ch = this.root.addChild(new MenuItem("E-shop 3D tisk", "e-shop-3d-tisk"));

		ch.addChild(new MenuItem("ABS", "abs"));
		ch.addChild(new MenuItem("PLA", "pla"));
		ch.addChild(new MenuItem("Nylon", "nylon"));

		this.root.addChild(new MenuItem("Košík", "kosik"));
	}

	public MenuItem getRoot() {
		return this.root;
	}

	public List<MenuItem> getRootChilds() {
		return this.root.getChilds();
	}

	public MenuItem findItem(String keyPath) {
		return this.findItem(this.root, keyPath);
	}

	private MenuItem findItem(MenuItem current, String keyPath) {

		String key = "";
		int idx;

		if ((idx = keyPath.indexOf("/")) >= 0) {
			key = keyPath.substring(0, idx);
			keyPath = keyPath.substring(idx + 1);

		} else {
			key = keyPath;
			keyPath = "";
		}

		for (MenuItem child : current.getChilds()) {
			if (child.getKey().equals(key)) {
				return keyPath.isEmpty() ? child : this.findItem(child, keyPath);
			}
		}

		return null;
	}

	public void mapRoutes(RouteConfiguration conf) {
		Stack<MenuItem> stk = new Stack<>();

		stk.push(this.root);

		while (!stk.isEmpty()) {
			MenuItem current = stk.pop();

			if (current == this.root) {
				// nothing

			} else {
				conf.setRoute(current.getKeyPath(), WebStandardLayoutView.class);
			}

			for (MenuItem sub : current.getChilds()) {
				stk.push(sub);
			}
		}

	}

}
