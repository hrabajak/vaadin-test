package com.packagename.myapp;

import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.WildcardParameter;
import org.springframework.beans.factory.annotation.Autowired;

public class WebMenuItemView extends WebStandardLayoutView implements HasUrlParameter<String> {

	public WebMenuItemView(@Autowired MenuCollector menu) {
		super(menu);
	}

	@Override
	public void setParameter(BeforeEvent event, @WildcardParameter String parameter) {

		if ((this.currentMenu = this.menu.findItem(parameter)) != null) {

			this.menuPanel.setCurrentMenu(this.currentMenu);
			this.navPanel.setMenuItemRebuild(this.currentMenu);

		} else {
			event.rerouteTo(WebNotFoundView.class);
		}
	}

}
