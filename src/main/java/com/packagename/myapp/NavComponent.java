package com.packagename.myapp;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.dom.Element;

import java.util.ArrayList;

@Tag("div")
public class NavComponent extends Component implements HasStyle {

	private ArrayList<NavItem> items;
	private NavItem defaultItem;
	private String headingCaption;

	public NavComponent() {
		this.items = new ArrayList<>();
		this.defaultItem = null;

		this.headingCaption = "";

		this.setClassName("nav");
	}

	public void addDefaultItem() {
		if (this.defaultItem == null) {
			this.items.add(this.defaultItem = new NavItem("Portál 3D tisku", ""));
		}
	}

	public void setHeadingCaption(String headingCaption) {
		this.headingCaption = headingCaption;
	}

	public void clean() {
		this.items.clear();;

		if (this.defaultItem != null) {
			this.items.add(this.defaultItem);
		}
	}

	public void addItem(NavItem item) {
		this.items.add(item);
	}

	public void setMenuItemRebuild(MenuItem item) {
		this.clean();

		while (item.getParent() != null) {
			this.items.add(this.defaultItem == null ? 0 : 1, new NavItem(item.getCaption(), item.getKeyPath()));

			item = item.getParent();
		}

		this.rebuild();
	}

	public void rebuild() {

		this.getElement().removeAllChildren();

		int pos = 0;
		int len = this.items.size();

		while (pos < len) {
			if (pos != 0) {
				Element spc = new Element("span");

				spc.setText(" > ");

				this.getElement().appendChild(spc);
			}

			this.items.get(pos).setActive(pos == len - 1);
			this.getElement().appendChild(this.items.get(pos).createElement());

			pos++;
		}

		Element h = new Element("h1");

		if (!this.headingCaption.isEmpty()) {
			h.setText(this.headingCaption);

		} else if (len > 0) {
			h.setText(this.items.get(len - 1).getCaption());
		}

		this.getElement().appendChild(h);
	}

}
