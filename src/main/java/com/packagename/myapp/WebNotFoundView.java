package com.packagename.myapp;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

public class WebNotFoundView extends WebStandardLayoutView {

	private Div contentDiv;

	public WebNotFoundView(@Autowired MenuCollector menu) {
		super(menu);

		String reqUrl = ((VaadinServletRequest)VaadinService.getCurrentRequest()).getHttpServletRequest().getRequestURI();

		this.contentDiv = new Div();
		this.contentDiv.getElement().setProperty("innerHTML", "<p>Stránka <strong>" + reqUrl + "</strong> nebyla nalezena!</p>");

		this.rightPanel.add(this.contentDiv);
	}

}
