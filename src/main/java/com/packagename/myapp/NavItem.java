package com.packagename.myapp;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.dom.DomEventListener;
import com.vaadin.flow.dom.Element;

public class NavItem {

	private String caption;
	private String url;
	private boolean active;

	public NavItem(String caption, String url) {
		this.caption = caption;
		this.url = url;
	}

	public Element createElement() {
		Element elm;

		elm = new Element("a");
		elm.setProperty("href", "/" + this.url);
		elm.setProperty("title", this.caption);
		elm.setText(this.caption);

		if (this.active) {
			elm.setAttribute("class", "active");
		}

		elm.addEventListener("click", (DomEventListener) event -> {

			Notification.show("Clicked: " + this.url);

			UI.getCurrent().navigate(this.url);

		})
		.addEventData("event.stopPropagation()")
		.addEventData("event.preventDefault()");

		return elm;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
