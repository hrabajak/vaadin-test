package com.packagename.myapp;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.dom.Element;

@Tag("div")
public class FooterComponent extends Component implements HasStyle {

	private Element par;

	public FooterComponent() {

		this.setClassName("foot");

		this.par = new Element("p");
		this.par.setProperty("innerHTML", "© the portál 3D tisku ČVUT FIT team");

		this.getElement().appendChild(this.par);
	}

}
