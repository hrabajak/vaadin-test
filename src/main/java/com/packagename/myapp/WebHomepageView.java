package com.packagename.myapp;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.dom.Element;

public class WebHomepageView extends WebHpLayoutView {

	private NavComponent navPanel;
	private NavComponent eshopNavPanel;

	private HorizontalLayout bt_lay;

	private Button bt_a;
	private Button bt_b;
	private Button bt_c;
	private Button bt_d;

	private HorizontalLayout eshop_bt_lay;

	public WebHomepageView() {
		super();

		this.navPanel = new NavComponent();
		this.navPanel.addDefaultItem();
		this.navPanel.setHeadingCaption("Služby tisku 3D modelů");
		this.navPanel.rebuild();

		this.bt_lay = new HorizontalLayout();
		this.bt_lay.setWidthFull();

		this.bt_a = new Button("Návrh modelů pro 3D tisk");
		this.bt_a.setWidthFull();
		this.bt_a.addClickListener(event -> {

			UI.getCurrent().navigate("navrh-modelu-pro-3d-tisk");

		});

		this.bt_b = new Button("Tisk vlastního 3D modelu");
		this.bt_b.setWidthFull();
		this.bt_b.addClickListener(event -> {

			UI.getCurrent().navigate("tisk-vlastniho-3d-modelu");

		});

		this.bt_c = new Button("Katalog reklamních předmětů");
		this.bt_c.setWidthFull();
		this.bt_c.addClickListener(event -> {

			UI.getCurrent().navigate("katalog-reklamnich-predmetu");

		});

		this.bt_lay.add(this.bt_a, this.bt_b, this.bt_c);
		this.getMiddle_first().add(this.navPanel, this.bt_lay);

		this.eshopNavPanel = new NavComponent();
		this.eshopNavPanel.setHeadingCaption("Nabídka příslušenství pro 3D tiskárny");
		this.eshopNavPanel.rebuild();

		this.eshop_bt_lay = new HorizontalLayout();
		this.eshop_bt_lay.setWidthFull();

		HorizontalLayout spc1 = new HorizontalLayout();
		spc1.setWidthFull();

		HorizontalLayout spc2 = new HorizontalLayout();
		spc2.setWidthFull();

		this.bt_d = new Button("Vstoupit do E-shopu");
		this.bt_d.setWidthFull();
		this.bt_d.addClickListener(event -> {

			UI.getCurrent().navigate("e-shop-3d-tisk");

		});

		this.eshop_bt_lay.add(spc1, spc2, this.bt_d);
		this.getMiddle_second().add(this.eshopNavPanel, this.eshop_bt_lay);

	}

}
