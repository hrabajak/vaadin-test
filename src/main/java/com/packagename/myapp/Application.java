package com.packagename.myapp;

import com.vaadin.flow.router.RouteConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public static ConfigurableApplicationContext context = null;

    public static void main(String[] args) {

        Application.context = SpringApplication.run(Application.class, args);

    }

}
