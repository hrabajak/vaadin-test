package com.packagename.myapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuItem implements Serializable {

	private MenuItem parent;
	private List<MenuItem> childs;

	private String caption;
	private String key;

	public MenuItem(String caption, String key) {
		this.childs = new ArrayList<>();

		this.caption = caption;
		this.key = key;
	}

	public MenuItem addChild(MenuItem child) {
		child.parent = this;
		this.childs.add(child);
		return child;
	}

	public MenuItem getParent() {
		return this.parent;
	}

	public List<MenuItem> getChilds() {
		return this.childs;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getKey() {
		return this.key;
	}

	public String getKeyPath() {
		String out = "";
		MenuItem current = this;

		while (current.parent != null) {
			if (out.isEmpty()) {
				out = current.key;
			} else {
				out = current.key + "/" + out;
			}

			current = current.parent;
		}

		return out;
	}

	public String getCaptionPath() {
		String out = "";
		MenuItem current = this;

		while (current.parent != null) {
			if (out.isEmpty()) {
				out = current.caption;
			} else {
				out = out + " | " + current.caption;
			}

			current = current.parent;
		}

		return out;
	}
}
