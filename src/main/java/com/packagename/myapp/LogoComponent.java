package com.packagename.myapp;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.dom.DomEventListener;
import com.vaadin.flow.dom.Element;

@Tag("div")
public class LogoComponent extends Component implements HasStyle {

	private Element href;

	public LogoComponent() {

		this.setClassName("logo");

		this.href = new Element("a");
		this.href.setAttribute("href", "/");
		this.href.setProperty("innerHTML", "<span>Portál 3D tisku</span>");

		this.href.addEventListener("click", (DomEventListener) event -> {

			UI.getCurrent().navigate("");

		})
		.addEventData("event.stopPropagation()")
		.addEventData("event.preventDefault()");

		this.getElement().appendChild(this.href);
	}

}
